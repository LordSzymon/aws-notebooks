import pprint

import requests


def handler(event, context):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(event)
    url = "https://jsonplaceholder.typicode.com/todos/1"
    response = requests.get(url)
    data = response.json()
    return data
