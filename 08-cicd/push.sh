#!/bin/sh

set -eu

if [ -z "${1:-}" ]; then
    echo "Usage: $0 <repository-name>"
    echo ""
    echo "  <name> - repository (image) name"
    exit 1
fi

aws() {
    docker run --rm \
        -v $HOME/.aws:/root/.aws \
        -e AWS_PROFILE \
        -e AWS_ACCESS_KEY_ID \
        -e AWS_SECRET_ACCESS_KEY \
        -e AWS_DEFAULT_REGION \
        amazon/aws-cli \
        "$@"
}

registry=514111127510.dkr.ecr.eu-central-1.amazonaws.com

docker_username=AWS
docker_password=$(aws ecr get-login-password --region eu-central-1)
docker login --username "$docker_username" --password "$docker_password" $registry

name=$registry/$1
version=$(git describe --tags --always)

docker tag lambda "$name:$version"
docker push "$name:$version"
