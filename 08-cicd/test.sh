#!/bin/sh

set -eu

container_id=$(docker run -d -p 8080 lambda)
port=$(docker port "$container_id" 8080 | cut -d: -f 2)

post() {
    HOST="$1"
    PORT="$2"
    POST_PATH="$3"
    BODY="$4"
    request=$(printf "POST %s HTTP/1.0\r\nHost: %s\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\n%s" "$POST_PATH" "$HOST" "${#BODY}" "$BODY")
    if ! echo "$request" | nc -w 15 "${HOST}" "${PORT}"; then
        echo >&2 "Fail to send POST to http://$HOST:$PORT$POST_PATH"
        return 1
    fi
    echo ""
}

# Strip the Date header since it always changes and fix EOL
post ${DOCKER_ADDRESS:-localhost} "${port}" "/2015-03-31/functions/function/invocations" "{}" | grep -v "^Date:" | sed 's/\r$//g' > response.http
cat <<EOF > expected.http
HTTP/1.0 200 OK
Content-Length: 73
Content-Type: text/plain; charset=utf-8

{"userId": 1, "id": 1, "title": "delectus aut autem", "completed": false}
EOF

if ! diff response.http expected.http; then
    echo "Container logs:"
    docker logs "$container_id"
    exit 1
fi

docker rm --force "$container_id"
